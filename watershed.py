import cv2
import numpy as np
import math
import matplotlib.pyplot as plt

#CONCEPT
#Preprocess an image to make lines more visible
#Create two images from input image with different threshld values for further analising
#Put watershed seeds on image and segment
#If the segmentd region is too small, put seds in a different layout so there are more of them
#Check in thresholded (bianrised) image weather thee road starts from bottom corner, if true, change layout to not have enviorement seed there
#Check in segmented image the pixel value on specific y for a certain number of x values to see where the segmened data is is, if in wrong palce, cahnge layout
#Check in thresholded (bianrised) image the pixel value on specific y for a certain number of x values to see where there is noise, and add a seed to it, because noise in the corners describes grass
#Use YOLO 3 to segment out people, and road signs, becausse the car should not drive into them
#Check in segmented region weather a central line can be found, if true, change layout to segment 2 lanes instead of one
#Check in thresholded (bianrised) image the pixel value on specific y for a certain number of x values to see where there are two very close continuous  x values describing a fense and lane line


def cropVert(img):
    #y,x = img.shape  # JPG
    y,x,a = img.shape # PNG
    cropImg = img[(y//2):y,0:x]
    #cropImg = img[(2*y//3):y,0:x]
    return cropImg

def binarize(imgG):
    cropG = cv2.cvtColor(imgG, cv2.COLOR_BGR2GRAY)
    threshImg = cv2.adaptiveThreshold(cropG,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,21,10)
    return threshImg

def avgN(prevPoints, new_line_coords, frame, initialFrame):
    frame = frame-initialFrame
    for j in range(0,16):
        if frame > 3:
            avgDeltaX = (abs(prevPoints[frame-4][j][0] - prevPoints[frame-3][j][0]) + abs(prevPoints[frame-3][j][0] - prevPoints[frame-2][j][0]) + abs(prevPoints[frame-2][j][0] - prevPoints[frame-1][j][0])) // 3
            lastDeltaX = abs(prevPoints[frame-1][j][0] - new_line_coords[j][0])
            avgDeltaY = (abs(prevPoints[frame-4][j][1] - prevPoints[frame-3][j][1]) + abs(prevPoints[frame-3][j][1] - prevPoints[frame-2][j][1]) + abs(prevPoints[frame-2][j][1] - prevPoints[frame-1][j][1])) // 3
            lastDeltaY = abs(prevPoints[frame-1][j][1] - new_line_coords[j][1])
            threshX = lastDeltaX / avgDeltaX
            threshY = lastDeltaY / avgDeltaY
            if (threshX == np.inf):
                threshX = 1.1
            if (threshY == np.inf):
                threshY = 1.1
            #print(threshX)
            avgX = ( prevPoints[frame-4][j][0] + prevPoints[frame-3][j][0] + prevPoints[frame-2][j][0] + prevPoints[frame-1][j][0] + new_line_coords[j][0] ) // 5
            avgY = ( prevPoints[frame-4][j][1]  + prevPoints[frame-3][j][1]  + prevPoints[frame-2][j][1] + prevPoints[frame-1][j][1] + new_line_coords[j][1] ) // 5
            #Blend values from previous frames with current frame
            # for n in range(0,3):
            #     avgX = (new_line_coords[j][0] + avgX)//2
            #     avgY = (new_line_coords[j][1] + avgY)//2
            if (threshX < 0.8 or threshX > 1.0):
                new_line_coords[j][0] = avgX
            if (threshY < 0.8 or threshY > 1.0):
                new_line_coords[j][1] = avgY
    return(new_line_coords)
#Road marking with watershed seeds
#In lane layout
def watershed_segmentation(seed_pos,seed_color,road_copy):
    road_copy.shape
    road_copy.shape[:2]
    marker_image = np.zeros(road_copy.shape[:2],dtype=np.int32)
    segments = np.zeros(road_copy.shape,dtype=np.uint8)
    segments.shape
    for i in range(0,len(seed_pos)):
        x = seed_pos[i][0]
        y = seed_pos[i][1]
        cv2.circle(marker_image, (x, y), 10, i, -1)# TRACKING FOR MARKERS
        cv2.circle(road_copy, (x, y), 10, seed_color[i], -1)# DISPLAY ON USER IMAGE
        cv2.circle(road_copy, (x, y), 10, (0,0,0), 2)# DISPLAY ON USER IMAGE
        #plt.imshow(marker_image),plt.show()
    marker_image_copy = marker_image.copy()
    cv2.watershed(road, marker_image_copy)
    segments = np.zeros(road.shape,dtype=np.uint8)
    for i in range(0,len(seed_pos)):
        segments[marker_image_copy == (i)] = seed_color[i]
    return segments


#Lane: single lane, seeds in pyramid, enviorement:6 side seeds, 4 top seeds
def seed_layout_1(road_copy):
    #NOTE! Last seed should have the color of (44,160,44) and will be used as to generatte contorus and cage,
    #if there is not a segmentation of this color, there will be an error "for i in range (0, len(contours[0])-1):IndexError: list index out of range"
    #Single lane layout
    lil_off_x =road_copy.shape[1]//32
    lil_off_y =road_copy.shape[0]//8
    seed_pos = np.array([[0,lil_off_y],#Top seeds
                [road_copy.shape[1],lil_off_y],
                [road_copy.shape[1]//4,lil_off_y],
                [road_copy.shape[1]*3//4,lil_off_y],
                [road_copy.shape[1]//2,lil_off_y],
                [lil_off_x,road_copy.shape[0]//4],#Left side seeds
                [lil_off_x,road_copy.shape[0]//2],
                [lil_off_x,road_copy.shape[0]*7//8],
                [road_copy.shape[1] - lil_off_x,road_copy.shape[0]//4],#Right side seeds
                [road_copy.shape[1] - lil_off_x,road_copy.shape[0]//2],
                [road_copy.shape[1] - lil_off_x,road_copy.shape[0]*7//8],
                [road_copy.shape[1]//2 - (road_copy.shape[1]//16),road_copy.shape[0] - lil_off_y],#Middlelane
                [road_copy.shape[1]//2 + road_copy.shape[1]//16,road_copy.shape[0] - lil_off_y],
                [road_copy.shape[1]//2,road_copy.shape[0] - lil_off_y]])
    seed_color = [(255,0,0), #BGR #Top seeds
                (255,0,0),
                (255,0,0),
                (255,0,0),
                (255,0,0),
                (255,0,0),#Left side seeds
                (255,0,0),
                (255,0,0),
                (255,0,0),#Right side seeds
                (255,0,0),
                (255,0,0),
                (44,160,44), #Middlelane
                (44,160,44),
                (44,160,44)]
    # x=road_copy.shape[1]
    # y=road_copy.shape[0]
    return seed_pos,seed_color

#Switching lanes layout
#Lane: Curently in left lane, switching to right
def seed_layout_2(road_copy):
        #NOTE! Last seed should have the color of (44,160,44) and will be used as to generatte contorus and cage,
        #if there is not a segmentation of this color, there will be an error "for i in range (0, len(contours[0])-1):IndexError: list index out of range"
        #Single lane layout
        lil_off_x =road_copy.shape[1]//32
        lil_off_y = road_copy.shape[0]//8
        lane_switch_offset = -1* road_copy.shape[1]//6
        seed_pos = np.array([[0,lil_off_y],#Top seeds
                    [road_copy.shape[1],lil_off_y],
                    [road_copy.shape[1]//4,lil_off_y],
                    [road_copy.shape[1]*3//4,lil_off_y],
                    [road_copy.shape[1]//2,lil_off_y],
                    [lil_off_x,road_copy.shape[0]//4],#Left side seeds
                    [lil_off_x,road_copy.shape[0]//2],
                    [lil_off_x,road_copy.shape[0]*7//8],
                    [road_copy.shape[1] - lil_off_x,road_copy.shape[0]//4],#Right side seeds
                    [road_copy.shape[1] - lil_off_x,road_copy.shape[0]//2],
                    [road_copy.shape[1] - lil_off_x,road_copy.shape[0]*7//8],
                    [road_copy.shape[1]//2 - (road_copy.shape[1]//16) + lane_switch_offset ,road_copy.shape[0] - lil_off_y],#Middlelane
                    [road_copy.shape[1]//2 + road_copy.shape[1]//16 + lane_switch_offset ,road_copy.shape[0] - lil_off_y],
                    [road_copy.shape[1]//2 + lane_switch_offset,road_copy.shape[0] - lil_off_y],
                    [road_copy.shape[1]//2 - (road_copy.shape[1]//16) - lane_switch_offset ,road_copy.shape[0] - lil_off_y],#Right side lane
                    [road_copy.shape[1]//2 + road_copy.shape[1]//16 - lane_switch_offset ,road_copy.shape[0] - lil_off_y],
                    [road_copy.shape[1]//2 - lane_switch_offset,road_copy.shape[0] - lil_off_y]])
        seed_color = [(255,0,0), #BGR #Top seeds
                    (255,0,0),
                    (255,0,0),
                    (255,0,0),
                    (255,0,0),
                    (255,0,0),#Left side seeds
                    (255,0,0),
                    (255,0,0),
                    (255,0,0),#Right side seeds
                    (255,0,0),
                    (255,0,0),
                    (44,160,44), #Middlelane
                    (44,160,44),
                    (44,160,44),
                    (255,255,0), #Right side lane
                    (255,255,0),
                    (255,255,0)]
        # x=road_copy.shape[1]
        # y=road_copy.shape[0]
        return seed_pos,seed_color
#Lane: Curently in right lane, switching to left
def seed_layout_3(road_copy):
        #NOTE! Last seed should have the color of (44,160,44) and will be used as to generatte contorus and cage,
        #if there is not a segmentation of this color, there will be an error "for i in range (0, len(contours[0])-1):IndexError: list index out of range"
        #Single lane layout
        lil_off_x =road_copy.shape[1]//32
        lil_off_y = road_copy.shape[0]//8
        lane_switch_offset = 1 * road_copy.shape[1]//6
        seed_pos = np.array([[0,lil_off_y],#Top seeds
                    [road_copy.shape[1],lil_off_y],
                    [road_copy.shape[1]//4,lil_off_y],
                    [road_copy.shape[1]*3//4,lil_off_y],
                    [road_copy.shape[1]//2,lil_off_y],
                    [lil_off_x,road_copy.shape[0]//4],#Left side seeds
                    [lil_off_x,road_copy.shape[0]//2],
                    [lil_off_x,road_copy.shape[0]*7//8],
                    [road_copy.shape[1] - lil_off_x,road_copy.shape[0]//4],#Right side seeds
                    [road_copy.shape[1] - lil_off_x,road_copy.shape[0]//2],
                    [road_copy.shape[1] - lil_off_x,road_copy.shape[0]*7//8],
                    [road_copy.shape[1]//2 - (road_copy.shape[1]//16) + lane_switch_offset ,road_copy.shape[0] - lil_off_y],#Middlelane
                    [road_copy.shape[1]//2 + road_copy.shape[1]//16 + lane_switch_offset ,road_copy.shape[0] - lil_off_y],
                    [road_copy.shape[1]//2 + lane_switch_offset,road_copy.shape[0] - lil_off_y],
                    [road_copy.shape[1]//2 - (road_copy.shape[1]//16) - lane_switch_offset ,road_copy.shape[0] - lil_off_y],#Left side lane
                    [road_copy.shape[1]//2 + road_copy.shape[1]//16 - lane_switch_offset ,road_copy.shape[0] - lil_off_y],
                    [road_copy.shape[1]//2 - lane_switch_offset,road_copy.shape[0] - lil_off_y]])
        seed_color = [(255,0,0), #BGR #Top seeds
                    (255,0,0),
                    (255,0,0),
                    (255,0,0),
                    (255,0,0),
                    (255,0,0),#Left side seeds
                    (255,0,0),
                    (255,0,0),
                    (255,0,0),#Right side seeds
                    (255,0,0),
                    (255,0,0),
                    (44,160,44), #Middlelane
                    (44,160,44),
                    (44,160,44),
                    (0,0,255), #Left side lane
                    (0,0,255),
                    (0,0,255)]
        # x=road_copy.shape[1]
        # y=road_copy.shape[0]
        return seed_pos,seed_color
#Lane: Curently in right lane, switching to left

def adjust_gamma(image, gamma=1.0):
   invGamma = 1.0 / gamma
   table = np.array([
      ((i / 255.0) ** invGamma) * 255
      for i in np.arange(0, 256)])
   return cv2.LUT(image.astype(np.uint8), table.astype(np.uint8))

#Needed for seg_intersect
def perp( a ) :
    b = np.empty_like(a)
    b[0] = -a[1]
    b[1] = a[0]
    return b

# line segment a given by endpoints a1, a2
# line segment b given by endpoints b1, b2
def seg_intersect(a1,a2, b1,b2) :
    da = a2-a1
    db = b2-b1
    dp = a1-b1
    dap = perp(da)
    denom = np.dot( dap, db)
    num = np.dot( dap, dp )
    return (num / denom.astype(float))*db + b1

def isBetween(a, b, c):# You want to know if c is between a and b
    #a[0]=x,a[1]=y
    crossproduct = (c[1] - a[1]) * (b[0] - a[0]) - (c[0] - a[0]) * (b[1] - a[1])
    # compare versus epsilon for floating point values, or != 0 if using integers
    if abs(crossproduct) < 0:
        return False
    dotproduct = (c[0] - a[0]) * (b[0] - a[0]) + (c[1] - a[1])*(b[1] - a[1])
    if dotproduct < 0:
        return False
    squaredlengthba = (b[0] - a[0])*(b[0] - a[0]) + (b[1] - a[1])*(b[1] - a[1])
    if dotproduct > squaredlengthba:
        return False
    return True


#LaneSwitch detection | Input is a image rgion whre to search, check if the region is overbrightened, and uses second image if so
    #divides region into boxes, returns the center of box where there is a lot of white
def lane_midle_line_detector(x1,x2,y1,y2,thresholded_image_1,thresholded_image_2):#Coordiantes of region whre to search, two image with differend threshold values
    #Getting values witch will be used to transform coordinates from given region space to full image space
    start_x1 = x1  # start_x2 = x2 Not nedede
    start_y1 = y1  # start_y2 = y2 Not nedede
    #Display Rectangle where the algorithm will search
    #cv2.rectangle(thresholded_image_1, (x1,y1), (x2,y2), (255, 255, 255), 2, 8, 0);
    #plt.imshow(thresholded_image_1,cmap='gray'),plt.show()
    #Getting a subimage, based on user input, shape[0]=y,shape[1]=x
    thresholded_image_selection = thresholded_image_1[y1:y2, x1:x2]
    #Check white disribution
    white_num = np.sum(thresholded_image_selection == 255)
    white_distibution = (thresholded_image_selection.shape[0]*thresholded_image_selection.shape[1])/white_num
    #print('white_distibution = ',white_distibution)
    if white_distibution <2: #If first thresholded image is overbrightened, use second one
        thresholded_image_selection = thresholded_image_2[y1:y2, x1:x2]
    #Morphological noise/artefact removal
    #kernel = np.ones((3,3), np.uint8)#Dilatation to fill noise in lanes
    #thresholded_image_selection = cv2.dilate(thresholded_image_selection, kernel, iterations=1)
    # kernel = np.ones((5,5), np.uint8)#Erode to remove outter artefatcts
    #thresholded_image_selection = cv2.erode(thresholded_image_selection, kernel, iterations=1)
    # kernel = np.ones((3,3), np.uint8)#Dilatation to fill noise in lanes
    # thresholded_image_selection = cv2.dilate(thresholded_image_selection, kernel, iterations=5)
    #Display
    #plt.imshow(thresholded_image_selection,cmap='gray'),plt.show()
    #Searching for very bright small subimage
    for i in range(0,4):
        x1 = 0+ i*thresholded_image_selection.shape[1]//4
        x2 = thresholded_image_selection.shape[1]//4 + i*thresholded_image_selection.shape[1]//4
        #print('x1,x2',x1,x2)
        #print('y1,y2',y1,y2)
        #Geting one of 4 quads of subimage image
        search_image = thresholded_image_selection[0:thresholded_image_selection.shape[0],x1:x2]#y1,y2,x1,x2, of region
        #Display
        #plt.imshow(search_image,cmap='gray'),plt.show()
        white_num = np.sum(search_image == 255)
        if white_num == 0: white_num = 0.1
        white_distibution = (search_image.shape[0]*search_image.shape[1])/white_num
        if (white_distibution == np.inf) or math.isnan(white_distibution): white_distibution = 2000
        #print(i)
        #print('white_distibution ',white_distibution)
        if 2 < white_distibution <200:#<2
            #print('x1,x2,y1,y2',x1,x2,y1,y2)
            #Transform (x2-x1,y2-y1) to original image coordiante system before return
            x1 = x1 + start_x1
            x2 = x2 + start_x1
            y1 =  start_y1
            y2 = thresholded_image_selection.shape[0] + start_y1
            #print(x1,y1)#Search box top left
            #print(x2,y2)#Search box bottom right
            #print('Found line in box with center at ',((x2-x1)/2)+x1,((y2-y1)/2)+y1)
            #plt.imshow(search_image,cmap='gray'),plt.show()
            #break
            #Return coordinates of center of found high white distribution
            return(((x2-x1)/2)+x1,((y2-y1)/2)+y1)
                #Return coordinates of center of found high white distribution
    print('No middle lane line found')
    return(0.0,0.0)

#_____________________________________________Iterative pipeline
def iterative_pipeline(road,frame,layout,previous_frames,out_img):
    #_________________Pre procssing (TODO move to code function and call if certain parameter is set)
    #road = cropVert(road)
    #road = adjust_gamma(road,1.5)#Adjust gamma of image, 0.1 if too road bright/noisy, 1.5 if image too dark
    #plt.imshow(road),plt.show()
    #sobelx = cv2.Sobel(road,cv2.CV_8U,1,0,ksize=5)
    # road_adaptive_binary = binarize(road)#extracts lanes as line
    # road_adaptive_binary=cv2.cvtColor(road_adaptive_binary, cv2.COLOR_GRAY2BGR)
    # road = cv2.addWeighted(road, 0.8, road_adaptive_binary, 0.5, 0)

    #Making two threshholded images
    road_thresh1 = cv2.cvtColor(road, cv2.COLOR_BGR2GRAY)
    ret,road_thresh1 = cv2.threshold(road_thresh1,150,255,cv2.THRESH_BINARY)
    #plt.imshow(road_thresh1,cmap='gray'),plt.show()
    road_thresh2 = cv2.cvtColor(road, cv2.COLOR_BGR2GRAY)
    ret,road_thresh2 = cv2.threshold(road_thresh2,100,255,cv2.THRESH_BINARY)
    #plt.imshow(road_thresh2,cmap='gray'),plt.show()
    #input ("DEBUG___________________")

    #Seraching conrents of thresholded image for lane line begining
    #Will be used to define wether a seed should be put in to bootom corner
    # y = marker_image.shape[0]-marker_image.shape[0]//4
    #y=0
    # for x in range(0,segments.shape[1]):
    #     #print(segments[y][x])
    #     if (segments[y][x] == [255, 127, 14]).all():
    #         print("Got segment at",x )
    #         #input ("DEBUG___________________")
    #         break

    #_________________Number of lanes and lane switching Checking (TODO call iterative_pipeline with certain layout and return its value after insted of continuing current function)
    #Upper marker
    x1,y1 = lane_midle_line_detector(int((14/32)*road_thresh1.shape[1]),int((17/32)*road_thresh1.shape[1]),int((11/18)*road_thresh1.shape[0]),int((12/18)*road_thresh1.shape[0]),road_thresh1,road_thresh2)#x1,x2,y1,y2,thresholded_image_1,thresholded_image_2
    # Display a marker on detected lane line
    cv2.circle(road_thresh2, (int(x1),int(y1)), 10, (0, 0, 0), 10),cv2.circle(road_thresh2, (int(x1),int(y1)), 10, (255, 255, 255), 4)
    cv2.circle(road_thresh1, (int(x1),int(y1)), 10, (0, 0, 0), 10),cv2.circle(road_thresh1, (int(x1),int(y1)), 10, (255, 255, 255), 4)
    #Lower marker
    x2,y2 = lane_midle_line_detector(int((14/32)*road_thresh1.shape[1]),int((17/32)*road_thresh1.shape[1]),int((14/18)*road_thresh1.shape[0]),int((17/18)*road_thresh1.shape[0]),road_thresh1,road_thresh2)#x1,x2,y1,y2,thresholded_image_1,thresholded_image_2
    # cv2.circle(road_thresh2, (int(x2),int(y2)), 10, (0, 0, 0), 10),cv2.circle(road_thresh2, (int(x2),int(y2)), 10, (255, 255, 255), 4)
    # cv2.circle(road_thresh1, (int(x2),int(y2)), 10, (0, 0, 0), 10),cv2.circle(road_thresh1, (int(x2),int(y2)), 10, (255, 255, 255), 4)
    # plt.imshow(road_thresh2,cmap='gray'),plt.show()
    # plt.imshow(road_thresh1,cmap='gray'),plt.show()
    #Creating image for drawing segmentations and colors for seeds (has only one channel instead of 3)
    road_copy = np.copy(road)
    #Draw a line segment connecting detected central lane lines
    if abs(x2 - x1)< 50 and x1 !=0.0 and x2 !=0.0:
        cv2.line(road_copy,(int(x1),int(y1)),(int(x2),int(y2)),(255,255,255),5)
        #Draw a infinite line through detected central lane lines
        a,b=0.0,0.0
        try:
            a =(1.0*(y2-y1))/(1.0*(x2-x1))
        except:
            a = 500000
        b = -a*x1+y1
        y1_ = int((0))
        y2_ = int((np.shape(road)[1]))
        x1_ = int(((y1_-b)/a))
        x2_ = int(((y2_-b)/a))
        cv2.line(road,(x1_,y1_),(x2_,y2_),(255,255,255),5)
        if (x2 + x1)//2 > road.shape[1]//2 :
            cv2.circle(road_copy, (road.shape[1]//2-100, 200), 5, (255, 0, 255), 5) #In right lane
            seed_pos,seed_color = seed_layout_2(road_copy)#In left lane switching to right
        else:
            #Detected middle lane left of car
            cv2.circle(road_copy, (road.shape[1]//2+100, 200), 5, (255, 0, 255), 5)#In left lane
            seed_pos,seed_color = seed_layout_3(road_copy)#In right lane switching to left
    else:
        seed_pos,seed_color = seed_layout_1(road_copy)#Single lane, no lane switching

    #_________________Watershed segmentations
    #Road segmentation marking with choosen watershed seed layout
    segments = watershed_segmentation(seed_pos,seed_color,road_copy)

    #_________________Segmentation check (TODO call iterative_pipeline with certain layout and return its value after insted of continuing current function)
    #Search for number of blue pixels in segmented image BGR[44,160,44}
    color_num = (segments == [44,160,44]).sum()
    color_distibution = (segments.shape[0]*segments.shape[1])/color_num
    #print(color_distibution)
    if color_distibution > 2:
        print("Segment too small, add seeds at frame ________", cifr)
        seed_pos = np.concatenate((seed_pos[:11], [[seed_pos[13][0],seed_pos[13][1]*7//8],[seed_pos[11][0]*7//8,seed_pos[11][1]],[seed_pos[11][0]*11//8,seed_pos[11][1]]], seed_pos[11:]))#Isert a position fo aditional seed
        seed_color[11:11] = (44,160,44),(44,160,44),(44,160,44)#Insert a color fo aditional seed
        segments = watershed_segmentation(seed_pos,seed_color,road_copy)



    #Search corners where the segmnet begins
    # y = marker_image.shape[0]-marker_image.shape[0]//4
    # for x in range(0,segments.shape[1]):
    #     #printsegments(segments[y][x])
    #     if (segments[y][x] == [255, 127, 14]).all():
    #         print("Got segment at",x )
    #         #input ("DEBUG___________________")
    #         break

    #_________________Segment extraction and calculation of fixed point polygon cage
    #cv2.imwrite('segments.jpg',segments)
    #cv2.imwrite('road_copy.jpg',road_copy)
    out_img = cv2.addWeighted(segments, 0.1, road_copy, 0.9, 0)
    road_copy = cv2.cvtColor(road_copy, cv2.COLOR_RGB2GRAY)
    road_copy = cv2.cvtColor(road_copy, cv2.COLOR_GRAY2RGB)
    #Extracting one segment as black and white image (can be used as mask when blending with original)
    #segments[np.where((segments==[255, 127, 14]).all(axis=2))] = [0,0,0]#Make bue pixels (Enviorement segment) black
    segments[np.where((segments!=[44,160,44]).all(axis=2))] = [0,0,0]#Make non green pixels black (will not be used for main lane cage)
    segments[np.where((segments==[44,160,44]).all(axis=2))] = [255,255,255]#Make green pixels white ((will be used for main lane cage))
    #plt.imshow(segments),plt.show()
    #Smoothing of single segment mask
    segments = cv2.blur(segments,(60,60))
    ret,segments = cv2.threshold(segments,150,255,cv2.THRESH_BINARY)
    kernel = np.ones((10,10), np.uint8)
    segments = cv2.dilate(segments, kernel, iterations=2)
    #plt.imshow(segments),plt.show()
    #Find convex hul of single segment mask
    segments = cv2.cvtColor(segments, cv2.COLOR_RGB2GRAY)
    contours, hierarchy = cv2.findContours(segments, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)# Finding contours for the thresholded image
    #hull = []# create hull array for convex hull points
    #Commented because in lane detectio nwe only want one contour, not multiple
    # for i in range(len(contours)):# calculate points for each contour
    #     hull.append(cv2.convexHull(contours[i], False))# creating convex hull object for each contour
    #print(hull[0])#Coordinates of convexHull points
    #print(hull[0][0])#First convexHull coordinate
    #hull = cv2.convexHull(contours[0], True)
    #print(len(hull))#=8
    #print(hull[3])
    line_coords=[]
    for i in range (0, len(contours[0])-1):
        if i==0:
            line_coords = contours[0][i][0]
        else:
            line_coords = np.vstack((line_coords,contours[0][i][0]))
    line_coords = np.vstack((line_coords,contours[0][0][0]))#Add the first coorinate at the end of stack
    #print(line_coords)
    cv2.polylines(out_img, np.int32([line_coords]), True, (128, 128, 128), thickness=3)
    drawing = np.zeros((segments.shape[0], segments.shape[1], 3), np.uint8)# create an empty black image
    # calculate moments for each contour
    M = cv2.moments(contours[0])
    # calculate x,y coordinate of center of hull
    cX = int(M["m10"] / M["m00"])
    cY = int(M["m01"] / M["m00"])
    center = np.array( [cX, cY] )
    cv2.circle(drawing, (cX, cY), 5, (255, 255, 255), -1)
    # print('contours \n',contours[0])
    # print('hull \n',hull)
    # print('line_coords \n',line_coords)
    # input ("DEBUG___________________?")
    #Defining a fixed poin poligon cage as an coordiante array witch will be used to replace convx hull
    x=road_copy.shape[1]
    y=road_copy.shape[0]
    #cage = np.array([[0,0],[x//2,0],[x,0],[0,y//2],[x,y//2],[0,y],[x//2,y],[x,y]])
    #Bottom, left corner, clockvise
    cage = np.array([[0,y],[0,int((2/3)*y)],[0,y//2],[0,int((1/3)*y)],[0,0],[int((1/3)*x),0],[x//2,0],[int((2/3)*x),0],[x,0],[x,int((1/3)*y)],[x,y//2],[x,int((2/3)*y)],[x,y],[int((2/3)*x),y],[x//2,y],[int((1/3)*x),y]])
    #print("Cage has ",len(cage),"points.\n","Cage coords are \n",cage,"\n and they love you <3")
    #Calcualting intersections betwen line connnecting a cage point to the center and a convex hull polygon
    #print(seg_intersect(center,top_right, top_right,center))
    new_line_coords=[]
    for i in range (0,len(cage)):
        line_forDraw = np.array([cage[i],center])
        cv2.polylines(drawing, np.int32([line_forDraw]), False, (0, 125, 125), thickness=2)
        cv2.circle(drawing, (int(cage[i][0]),int(cage[i][1])), 5, (255, 0, 0), -1)
        #input ("Debug!___________________?")
        for e in range (0,len(line_coords)-1):
            try:
                bound_point = seg_intersect(cage[i],center, line_coords[0+e],line_coords[1+e])
                line_forDraw = np.array([line_coords[0+e],line_coords[1+e]])
                cv2.polylines(drawing, np.int32([line_forDraw]), False, (0, 255, 0), thickness=2)
                if isBetween(cage[i], center, bound_point) and isBetween(line_coords[0+e], line_coords[1+e], bound_point):
                    cv2.circle(drawing, (int(bound_point[0]),int(bound_point[1])), 5, (255, 0, 255), -1)
                    if i==0:
                        new_line_coords = bound_point
                        break
                    else:
                        new_line_coords = np.vstack((new_line_coords,bound_point))
                        break
            except:
                print("Frame has invalid coordinate values")
    while len(new_line_coords) < 16:
        new_line_coords = np.vstack((new_line_coords,[x//2,y]))

    #print('cage ',len(cage))
    #print('new_line_coords ',len(new_line_coords))
    #Filterubg new_line_coords looking up last 4 frames
    new_line_coords = avgN(previous_frames, new_line_coords, frame, initialFrame)
    cv2.polylines(out_img, np.int32([new_line_coords]), True, (0, 255, 0), thickness=1)
    #print(len(new_line_coords))
    #Lane drawing, left=blue, right=red
    #left_line_coords=new_line_coords[0:4]
    #right_line_coords=new_line_coords[4:7]
    left_line_coords=new_line_coords[0:5]
    right_line_coords=new_line_coords[9:13]
    cv2.polylines(out_img, np.int32([left_line_coords]), False, (0, 0, 255), thickness=4)
    cv2.polylines(out_img, np.int32([right_line_coords]), False, (255, 0, 0), thickness=4)
    segments = cv2.cvtColor(segments, cv2.COLOR_GRAY2RGB)
    #out_img = cv2.addWeighted(out_img, 0.8, segments, 0.2, 0)
    return (new_line_coords,out_img)


#_____________________________________________Input values
initialFrame = 0 #Initial frame 0 #Lane switching happening at f 240
lastFrame= 1169#1169
layout=1 #currently doesnt have affect on code
previous_frames=[]
#_____________________________________________Main loop
for frame in range(initialFrame, lastFrame, 1):
    cifr = ((10 -  len(str(frame))) * "0") + str(frame)

    road = cv2.imread('/media/sigmoid/HardDisk1TB/Datasets/KITTI/2011_10_03/2011_10_03_drive_0042_sync/image_02/data/'+cifr+'.png')
    out_img = road
    #Initiate iterative pipeline for current frame
    new_line_coords,out_img = iterative_pipeline(road,frame,layout,previous_frames,out_img)
    #print('previous_frames',previous_frames)
    #print('new_line_coords',new_line_coords)
    #print(len(new_line_coords))
    if frame == initialFrame:
        previous_frames = np.array([new_line_coords])
    else:
        previous_frames = np.vstack((previous_frames,np.array([new_line_coords])))

    #print(previous_frames[0][0][0])
    #print('previous_frames \n',previous_frames)
    #cv2.imwrite('results/'+cifr + '.png', road_thresh2)
    #cv2.imwrite('results/'+cifr + '.png', drawing)
    # plt.imshow(out_img),plt.show()
    # input ("WRITE___________________?")
    cv2.imwrite('results/'+cifr + '.png', out_img)
    print('Saving image... ',cifr )
