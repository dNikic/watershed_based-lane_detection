# Watershed lane detection and segmentation
## Camera space lane detection and segmentation

### About:
This algorithm  segments lanes and detects lane lines on given iamges.
For more details refer to Watershed_Diagram.html

### Requirements:
* python 3
* opencv 2
* matplotlib
* numpy
* math

### Usage:
Run watershed.py to run the algorithm.
Input parameters can be found by searching for the "Input values" comment of watershed.py.

If you wish to use the version with manual seed placement, clone the commit defined with 4d793be67357fc0c0caca33dac349031905bcde2
It is suitable for testing and discovering alternative seed layouts.
